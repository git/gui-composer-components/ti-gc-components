**_HTML_**
```
<gc-widget-led id="demo-element"></gc-widget-led>
```

**_CSS_**
```
#demo-element {
    --gc-on-color: green;
    --gc-off-color: red;
}
```