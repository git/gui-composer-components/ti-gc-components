**_HTML_**
```
<gc-widget-cloudagent-dialog />
```

**_JavaScript_**

If the agent.js is not loaded, one can load it using this script tag. The agent.js script is automatically loaded by the `BackplaneService`.

```
<script src="/ticloudagent/agent.js"></script>
```