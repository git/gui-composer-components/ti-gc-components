```
<gc-model-streaming id="streaming"></gc-model-streaming>

<gc-codec-json id="json"></gc-codec-json>
<gc-codec-delimited-text id="cr"></gc-codec-delimited-text>
<gc-transport-usb id="usb" usb></gc-transport-usb>
<gc-target-connection-manager auto-connect active-configuration="usb+cr+json+streaming"></gc-target-connection-manager>
```