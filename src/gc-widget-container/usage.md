**_HTML_**

Flex row and column **tile** with center alignment
```
<gc-widget-container elevation="1" heading="My Container" layout horizontal center-justified center></gc-widget-container>
<gc-widget-container elevation="1" heading="My Container" layout vertical center-justified center></gc-widget-container>
```

Absolute XY Layout
```
<gc-widget-container></gc-widget-container>
```

Flex Row Layout
```
<gc-widget-container layout horizontal></gc-widget-container>
```

Flex Column Layout
```
<gc-widget-container layout vertical></gc-widget-container>
