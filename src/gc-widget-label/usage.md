**_HTML_**
```
<gc-widget-label id="demo-element" label="Hello World!" />
```

**_CSS_**
```
#demo-element {
    --gc-color: blue;
    --gc-hover-color: lightblue;
    --gc-hover-cursor: pointer;
```