```
<gc-codec-base64 id="base64"></gc-codec-base64>

<gc-model-streaming id="stream"></gc-model-streaming>
<gc-transport-usb id="usb" usb default-baud-rate="9600"></gc-transport-usb>
<gc-target-connection-manager auto-connect active-configuration="usb+base64+custom+stream"></gc-target-connection-manager>
```
