/**
 *  Copyright (c) 2020, 2021 Texas Instruments Incorporated
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *  *   Redistributions of source code must retain the above copyright
 *  notice, this list of conditions and the following disclaimer.
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  *   Neither the name of Texas Instruments Incorporated nor the names of
 *  its contributors may be used to endorse or promote products derived
 *  from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 *  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 *  PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 *  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 *  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import { Component, h, Prop, Event, EventEmitter, Listen, Watch, Element, Method } from '@stencil/core';
import { WidgetBaseCheck } from '../gc-widget-base/gc-widget-base-check';
import { WidgetBaseCheckProps } from '../gc-widget-base/gc-widget-base-check-props';
import { ValueType } from '../gc-widget-base/gc-widget-base-selector';
import { WidgetBaseDisabledProps } from '../gc-widget-base/gc-widget-base-disabled-props';
import { WidgetBaseTooltipProps } from '../gc-widget-base/gc-widget-base-tooltip-props';

/**
 * `gc-widget-radio` is a toggle button in the radio style.  It can be used on it's own, or added to a
 * gc-widget-radio-group for mutually exclusive options.
 *
 * @label Radio Button
 * @group Inputs
 * @demo
 * @usage
 * @archetype <gc-widget-radio label="Radio Button"></gc-widget-radio>
 */
@Component({
    tag: 'gc-widget-radio',
    styleUrl: 'gc-widget-radio.scss',
    shadow: true
})
export class WidgetRadio implements WidgetBaseCheckProps, WidgetBaseTooltipProps, WidgetBaseDisabledProps {
    private base = new (
        class extends WidgetBaseCheck {
            get radio() {
                return this.parent as WidgetRadio;
            }

            get element() {
                return this.radio.el;
            }
        }
    )(this);

    /**
     * The radio button value.
     * @order 2
     */
    @Prop({}) value: ValueType;

    /**
     * Fired when the `disabled` property has changed.
     */
    @Event({ eventName: 'disabled-changed' }) disabledChanged: EventEmitter<{ value: boolean }>;

    /**
     * Fired when the `value` property has changed.
     */
    @Event({ eventName: 'value-changed' }) valueChanged: EventEmitter<{ value: ValueType }>;

    componentWillLoad() {
        this.el.classList.add('ti-radio');
        this.value = this.value  || `${this.el.getAttribute('id')}`;
        if (this.el.parentElement.attributes.getNamedItem('horizontal')) {
            this.el.classList.add('horizontal');
        }
    }

    render() {
        // JSXON
        return this.base.render(
            <ti-radio
                value={ this.value }
                disabled={ this.disabled }
                selected={ this.checked }
            >
                { this.base.renderCheck() }
            </ti-radio>,
            { tooltip: this.tooltip }
        );
        // JSXOFF
    }

    @Listen('tiChange')
    onTiChanged(event: CustomEvent<{ selected: boolean; value: ValueType }>) {
        this.checked = event.detail.selected;
        this.value = event.detail.value;
        this.checkedChanged.emit({ value: this.checked });
    }

    @Watch('checked')
    onCheckedChanged(newValue: boolean) {
        this.checkedChanged.emit({ value: newValue });
    }

    @Watch('disabled')
    onDisabledChanged(newValue: boolean) {
        this.disabledChanged.emit({ value: newValue });
    }

    // #region gc-widget-base/gc-widget-base-check-props.tsx:
    // -----------Autogenerated - do not edit--------------
    /**
     * If true, the widget is checked.
     * @order 2
     */
    @Prop({ reflect: true }) checked: boolean = false;

    /**
     * Fired when the `checked` property has changed.
     */
    @Event({ eventName: 'checked-changed' }) checkedChanged: EventEmitter<{ value: boolean }>;

    /**
     * The label to display
     * @order 3
     */
    @Prop({ reflect: true }) label: string;

    /**
     * The label to display when check state is checked.
     * @order 4
     */
    @Prop({ reflect: true }) labelWhenChecked: string;
    // #endregion
    // #region gc-widget-base/gc-widget-base-props.tsx:
    // -----------Autogenerated - do not edit--------------
    /**
     * The widget element.
     */
    @Element() el: HTMLElement;

    /**
     * Sets to `true` to hide the element, otherwise `false`.
     *
     * @order 200
     */
    @Prop({ reflect: true }) hidden: boolean = false;

    /**
     * Fired when a CSS property has changed.
     **/
    @Event({ eventName: 'css-property-changed' }) cssPropertyChanged: EventEmitter<{ name: string; value: string }>;

    /**
     * Sets the CSS property.
     *
     * @param {string} name the element style name
     * @param {string} value the new CSS property to be set
     */
    @Method()
    async setCSSProperty(name: string, value: string): Promise<void> {
        value = value.replace(/^[ ]+|[ ]+$/g, '');
        if (await this.getCSSProperty(name) !== value) {
            this.el.style.setProperty(name, value);
            this.cssPropertyChanged.emit({ name: name, value: value });
        }
    }

    /**
     * Returns the value of a CSS property.
     *
     * @param {string} name the element style property
     * @returns {string} the value of the property
     */
    @Method()
    async getCSSProperty(name: string): Promise<string> {
        return getComputedStyle(this.el).getPropertyValue(name).trim();
    }

    /**
     * Refresh the widget.
     */
    @Method()
    async refresh(): Promise<void> {
        return this.el['forceUpdate']();
    }
    // #endregion
    // #region gc-widget-base/gc-widget-base-tooltip-props.tsx:
    // -----------Autogenerated - do not edit--------------
    /**
     * Controls the tooltip that is displayed for this widget.
     * @order 210
     */
    @Prop() tooltip: string;
    // #endregion
    // #region gc-widget-base/gc-widget-base-disabled-props.tsx:
    // -----------Autogenerated - do not edit--------------
    /**
     * Controls the widget disabled state.
     * @order 202
     */
    @Prop({ reflect: true }) disabled: boolean = false;
    // #endregion

}
