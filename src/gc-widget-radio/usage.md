**_HTML_**
```
<gc-widget-radio-group selected-index="0" horizontal>
    <gc-widget-radio id="small" label="Small"></gc-widget-radio>
    <gc-widget-radio id="medium" label="Medium" disabled></gc-widget-radio>
    <gc-widget-radio id="large" label="Large"></gc-widget-radio>
</gc-widget-radio-group>
```