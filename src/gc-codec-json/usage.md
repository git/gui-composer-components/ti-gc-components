```
<gc-codec-json id="json"></gc-codec-json>

<gc-codec-delimited-text id="cr"></gc-codec-delimited-text>
<gc-model-streaming id="stream"></gc-model-streaming>
<gc-transport-usb id="usb" usb></gc-transport-usb>
<gc-target-connection-manager auto-connect active-configuration="usb+cr+json+stream"></gc-target-connection-manager>
```