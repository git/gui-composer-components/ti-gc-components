## The following is done in Markdown. See the [Markdown Basics Guide](https://help.github.com/articles/markdown-basics/) for info on Markdown
        

* On-board XDS110 for flashing & debugging firmware on the CC1310 device.
* 40-pin dual-gender BoosterPack connectors
CC1310 Wireless MCU
Red & green LED
* 2x buttons
* Access to all pins of the CC1310 device
* Connector for external antenna
* 8Mbit external serial (SPI) Flash memory

**Power supply requirements:**

The LaunchPad is designed to be powered from a USB-compliant power source, either a USB charger or a computer. When used this way, jumpers need to be mounted on the 3V3 position of the central jumper block. An LDO powered from the USB VBUS supply supplies 3.3V to the XDS debugger, the CC1310, and associated circuitry including the 3V3-marked pins for BoosterPacks.

**Temperature range:**

The LaunchPad is designed for operation -25 to +70 C. Note that other BoosterPacks and LaunchPads may have different temperature ranges, and when combined, the combination will be set by the most restrictive combined range."