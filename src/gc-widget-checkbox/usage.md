**_HTML_**
```
<gc-widget-checkbox label="Click Me!" checked></gc-widget-checkbox>
<gc-widget-checkbox label="Disabled" disabled></gc-widget-checkbox>
<gc-widget-checkbox label="Off" label-when-checked="On"></gc-widget-checkbox>
```