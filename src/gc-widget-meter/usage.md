**_HTML_**
```
<gc-widget-meter value="42" main-title="Speedometer" sub-title="km" />
<gc-widget-meter value="42" min-value="0" max-value="100" />
<gc-widget-meter value="42" precision="1" />

```

**_CSS_**
```
#demo_element {
    --gc-arc-thickness: thin;
    --gc-arc-background-color: lightgray;
    --gc-arc-low-color: red;
    --gc-arc-mid-color: orange;
    --gc-arc-high-color: green;
```