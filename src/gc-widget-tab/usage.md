**_HTML_**
```
<gc-widget-tab-container>
    <gc-widget-link-tab-panel id="intro" href="./intro.html" label="Intro" icon-name="icons:home" />
    <gc-widget-tab-panel id="search" label="Search" icon-name="icons:search" tooltip="Search the internet" has-heading />
    <gc-widget-tab-panel id="report" label="Report" icon-name="icons:report" tooltip="But report" has-heading />
</gc-widget-tab-container>
```