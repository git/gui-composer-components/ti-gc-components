export interface TestResults {
    myStruct: string;
    _charArray: string;
    _charPtr: string;
    _charPtr_value: string;
    _int: string;
    _bool: string;
    connectionName: string;
    debugPath: string;
    coreName: string;
}